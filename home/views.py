from django.shortcuts import render, redirect

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


def home(request):
    return render(request, 'home.html')


def register_user(request):

    if request.POST:
        first_name = request.POST.get('txt_first_name')
        last_name = request.POST.get('txt_last_name')
        user_name = request.POST.get('txt_user_name')
        password = request.POST.get('txt_password')

        objUser = User()
        objUser.first_name = first_name
        objUser.last_name = last_name
        objUser.username = user_name
        objUser.set_password(password)
        objUser.save()

    return render(request, 'register.html')


def logout_user(request):
    logout(request)
    return redirect('/')


def login_user(request):

    error_msg = None
    redirect_url = request.GET.get('next', None)
    print(redirect_url)
    if request.POST:
        user_name = request.POST.get('txt_user_name')
        password = request.POST.get('txt_password')
        redirect_url = request.POST.get('hdn_next')
        print(redirect_url)
        if not user_name:
            error_msg = 'Please enter your username'
            return render(request, 'login.html', {'error_msg':  error_msg})
        if not password:
            error_msg = 'Please enter your password'
            return render(request, 'login.html', {'error_msg':  error_msg})

        obj_user = authenticate(password=password, username=user_name)
        if obj_user:
            # Login successful
            login(request, obj_user)
            if redirect_url:
                return redirect(redirect_url)

            return redirect('/')
        else:
            # login failed (wrong username or password)
            error_msg = 'Wrong username or password'

    return render(request, 'login.html', {'error_msg':  error_msg, 'redirect_url': redirect_url})
