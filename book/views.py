from django.shortcuts import render, redirect
from book.models import Book

# Create your views here.


def book_info(request):
    return render(request, 'book_info.html')


def show_save_book(request):
    print("show save data")
    return render(request, 'add_book.html')


def save_book_data(request):
    book_name = request.POST.get('txt_book_name')
    author_name = request.POST.get('txt_author_name')
    publisher_name = request.POST.get('txt_publisher_name')

    print('Book name : ', book_name)
    print('author_name : ', author_name)
    print('publisher_name : ', publisher_name)

    obj_book = Book()
    obj_book.author = author_name
    obj_book.book_name = book_name
    obj_book.publisher = publisher_name
    obj_book.save()

    return redirect('/show-books/')


def get_all_books(request):
    lst_books = Book.objects.all()
    return render(request, 'show_books.html', {'books': lst_books})


def get_one_books(request):
    book_id = request.GET.get('id')

    obj_book = Book.objects.get(pk=int(book_id))
    return render(request, 'show_book.html', {'obj_book': obj_book})


def delete_book(request):
    book_id = request.GET.get('id')
    obj_book = Book.objects.get(pk=int(book_id))
    obj_book.delete()
    return redirect('/show-books/')


def show_edit_book(request):
    book_id = request.GET.get('id')
    obj_book = Book.objects.get(pk=int(book_id))
    return render(request, 'edit_book.html', {'obj_book': obj_book})


def edit_book_data(request):
    book_name = request.POST.get('txt_book_name')
    author_name = request.POST.get('txt_author_name')
    publisher_name = request.POST.get('txt_publisher_name')
    book_id = request.POST.get('hdn_book_id')

    obj_book = Book.objects.get(pk=int(book_id))
    obj_book.author = author_name
    obj_book.book_name = book_name
    obj_book.publisher = publisher_name

    obj_book.save()

    return redirect('/show-books/')

