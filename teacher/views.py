from django.shortcuts import render, redirect
from .models import Teacher
from django.contrib.auth.decorators import login_required


@login_required(login_url='/login')
def view_teachers(request):
    teacher_id = request.GET.get('id', None)
    page_data = None
    if teacher_id:
        # When teach id is sent in url (query string)
        teacher_obj = get_teacher_object(teacher_id)
        page_data = {'obj_teacher': teacher_obj}
    else:
        # When teach id is not sent in url (query string)
        list_teacher = Teacher.objects.all
        page_data = {'teachers': list_teacher}

    return render(request, 'show_teachers.html', page_data)


def get_teacher_object(teacher_id):
    obj_teacher = Teacher.objects.get(pk=int(teacher_id))
    return obj_teacher


@login_required(login_url='/login')
def delete_teacher(request):
    teacher_id = request.GET.get('id', None)
    obj_teacher = get_teacher_object(teacher_id)
    obj_teacher.delete()
    return redirect('/view-teachers/')


@login_required(login_url='/login')
def add_teacher(request):
    page_data = None

    if request.GET:
        print("GET Request")
        # It will run only when it's a GET request
        teacher_id = request.GET.get('id', None)
        if teacher_id:
            teacher_obj = get_teacher_object(teacher_id)
            page_data = {'obj_teacher': teacher_obj}

    if request.POST:
        # It will run only when it's a POST request
        teacher_id = request.POST.get('hdn_teacher_id', None)
        teacher_name = request.POST.get('txt_teacher_name')
        teacher_age = request.POST.get('txt_teacher_age')
        teacher_salary = request.POST.get('txt_teacher_salary')
        if teacher_id:
            Teacher(id=int(teacher_id), teacher_name=teacher_name, age=int(teacher_age),
                    salary=teacher_salary).save()
        else:
            Teacher(teacher_name=teacher_name, age=int(teacher_age),
                    salary=teacher_salary).save()

        return redirect('/view-teachers/')

    return render(request, 'add_teacher.html', page_data)
