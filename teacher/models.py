from django.db import models

# Create your models here.


class Teacher(models.Model):
    id = models.IntegerField(primary_key=True)
    teacher_name = models.CharField(max_length=50)
    age = models.IntegerField()
    salary = models.CharField(max_length=50)
