
from django.contrib import admin
from django.urls import path
from book import views as book_views
from teacher import views as teacher_views
from home import views as home_views

urlpatterns = [

    path('admin/', admin.site.urls),

    # Home page url
    path('', home_views.home),

    # Auth Urls
    path('register/', home_views.register_user),
    path('login/', home_views.login_user),
    path('logout/', home_views.logout_user),


    path('book/', book_views.book_info),
    path('add-book/', book_views.show_save_book),
    path('save-book-data/', book_views.save_book_data),
    path('show-books/', book_views.get_all_books),
    path('show-book/', book_views.get_one_books),
    path('delete-book/', book_views.delete_book),
    path('edit-book/', book_views.show_edit_book),
    path('edit-book-data/', book_views.edit_book_data),

    # Teacher Urls
    path('view-teachers/', teacher_views.view_teachers),
    path('add-teacher/', teacher_views.add_teacher),
    path('delete-teacher/', teacher_views.delete_teacher),

]
